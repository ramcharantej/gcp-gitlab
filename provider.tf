provider "google" {
  credentials = "${file("./creds/serviceaccount.json")}"
  project = "consummate-atom-219206"
  region  = "us-central1"
}
