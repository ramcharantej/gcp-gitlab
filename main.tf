resource "google_compute_instance" "vm_instance" {
  name         = "terraform-instance1"
  machine_type = "f1-micro"
  zone         = "us-central1-c"
  tags = ["db"]

  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-1804-lts"
    }
  }

  // Local SSD disk

  network_interface {
    network = "default"

    access_config {
      // Ephemeral IP
    }
  }
  metadata {
   sshKeys = "root:${file("~/.ssh/myterragcp.pub")}"
 }
 
  provisioner "file" {
        source      = "scripts/install.sh"
        destination = "/tmp/install.sh"
    }
  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/install.sh",
      "sh /tmp/install.sh",
    ]
  }
  connection {
     type = "ssh"
     user = "root"
     private_key = "${file("~/.ssh/myterragcp")}"
    }
#  resource "google_compute_network" "vpc_network" {
 # 	name                    = "terraform-network"
  #	auto_create_subnetworks = "true"
##	}

}
