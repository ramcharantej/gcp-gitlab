terraform {
  backend "gcs" {
    bucket      = "tf-demo-tfstate"
    credentials = "./creds/serviceaccount.json"
  }
}
